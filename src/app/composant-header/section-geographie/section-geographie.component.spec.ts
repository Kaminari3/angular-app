import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionGeographieComponent } from './section-geographie.component';

describe('SectionGeographieComponent', () => {
  let component: SectionGeographieComponent;
  let fixture: ComponentFixture<SectionGeographieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SectionGeographieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionGeographieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
