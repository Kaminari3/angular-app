import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuFormulaireComponent } from './menu-formulaire.component';

describe('MenuFormulaireComponent', () => {
  let component: MenuFormulaireComponent;
  let fixture: ComponentFixture<MenuFormulaireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuFormulaireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuFormulaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
