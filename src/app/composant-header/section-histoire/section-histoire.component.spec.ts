import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionHistoireComponent } from './section-histoire.component';

describe('SectionHistoireComponent', () => {
  let component: SectionHistoireComponent;
  let fixture: ComponentFixture<SectionHistoireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SectionHistoireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionHistoireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
