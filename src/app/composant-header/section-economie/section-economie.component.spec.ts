import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionEconomieComponent } from './section-economie.component';

describe('SectionEconomieComponent', () => {
  let component: SectionEconomieComponent;
  let fixture: ComponentFixture<SectionEconomieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SectionEconomieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionEconomieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
