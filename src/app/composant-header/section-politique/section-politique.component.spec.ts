import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionPolitiqueComponent } from './section-politique.component';

describe('SectionPolitiqueComponent', () => {
  let component: SectionPolitiqueComponent;
  let fixture: ComponentFixture<SectionPolitiqueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SectionPolitiqueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionPolitiqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
