import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { EntetePageComponent } from './composant-header/entete-page/entete-page.component';
import { SectionHistoireComponent } from './composant-header/section-histoire/section-histoire.component';
import { SectionGeographieComponent } from './composant-header/section-geographie/section-geographie.component';
import { SectionPolitiqueComponent } from './composant-header/section-politique/section-politique.component';
import { SectionEconomieComponent } from './composant-header/section-economie/section-economie.component';
import { MenuFormulaireComponent } from './composant-header/menu-formulaire/menu-formulaire.component';
import { MenuSideComponent } from './composant-header/menu-side/menu-side.component';
import { FormsModule } from '@angular/forms';
import { GreenSectionTitleDirective } from './green-section-title.directive';
import { CountryCodePipe } from './country-code.pipe';


@NgModule({
  declarations: [
    AppComponent,
    EntetePageComponent,
    SectionHistoireComponent,
    SectionGeographieComponent,
    SectionPolitiqueComponent,
    SectionEconomieComponent,
    MenuFormulaireComponent,
    MenuSideComponent,
    GreenSectionTitleDirective,
    CountryCodePipe
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
