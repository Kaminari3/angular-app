import { Directive, ElementRef ,HostListener} from '@angular/core';

@Directive({
  selector: '[appGreenSectionTitle]'
})
export class GreenSectionTitleDirective {

  constructor(private el: ElementRef) {

  	//el.nativeElement.style.backgroundColor = 'green';

  }


  @HostListener('mouseenter') onMouseEnter() {
    this.highlight('green','x-large');
  }
 
  @HostListener('mouseleave') onMouseLeave() {
    this.highlight(null,null);
  }
 
  private highlight(color: string,fontSize:string) {
    this.el.nativeElement.style.backgroundColor = color;
    this.el.nativeElement.style.fontSize = fontSize;
  }

}
