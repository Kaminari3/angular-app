import { Pipe, PipeTransform } from '@angular/core';




@Pipe({
  name: 'countryCode'
})
export class CountryCodePipe implements PipeTransform {

  transform(code:Number): string {

    return `sn${code}`;
  }

}
